package streaming.hamid.app.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;

public class SessionManager {

    private String TAG = "WordGuess";

    public static final String KEY_NAME = "name";
    public static final String KEY_EMAIL = "email";
    private static final String SHARED_PREFER_FILE_NAME = "wordguess";
    private static final String IS_USER_LOGGED_IN = "IsUserLoggedIn";
    public static final String KEY_UID = "uid";

    SharedPreferences pref;
    Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(SHARED_PREFER_FILE_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    //////Create sessions

    public void createUserLoginSession(String email, String name, String uid) {
        editor.putBoolean(IS_USER_LOGGED_IN, true);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_UID, uid);
        editor.commit();
    }


    ////////////////Check for sessions
    public boolean isUserLoggedIn() {
        return pref.getBoolean(IS_USER_LOGGED_IN, false);
    }


    public boolean checkLogin() {
        if (this.isUserLoggedIn()) {
            return true;
        }
        return false;
    }

///////////////////////////// end of checking for sessions


    //////////////Get the data
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_UID, pref.getString(KEY_UID, null));
        return user;
    }

///////////////End of getting data


    //////////////delete session
    public void logoutUser() {
        editor.clear();
        editor.commit();
      /*  Intent i = new Intent(_context, SplashScreenActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i); */
    }
}