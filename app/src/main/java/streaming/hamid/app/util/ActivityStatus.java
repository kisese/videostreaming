package streaming.hamid.app.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Brayo on 5/9/2016.
 */
public class ActivityStatus {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;

    private static final String SHARED_PREFER_FILE_NAME = "active";

    public ActivityStatus(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(SHARED_PREFER_FILE_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setStatus(Boolean activity_status) {
        editor.putBoolean("activity_status", activity_status);
        editor.commit();
    }


    public Boolean getStatus(){
        return pref.getBoolean("activity_status", false);
    }

}
