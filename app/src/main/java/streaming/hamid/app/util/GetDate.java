package streaming.hamid.app.util;

import java.util.Calendar;

/**
 * Created by Brayo on 7/9/2015.
 */
public class GetDate {
    private String ampm;
    private String date;
    private String a;

    Calendar c = Calendar.getInstance();
    int day = c.get(Calendar.DATE);
    int month = c.get(Calendar.MONTH);
    int year = c.get(Calendar.YEAR);
    int time = c.get(Calendar.HOUR);
    int min = c.get(Calendar.MINUTE);
    int am = c.get(Calendar.AM_PM);

    public String getTimeStamp() {
        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        return ts;
    }
}
