package streaming.hamid.app.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Brayo on 4/12/2016.
 */
public class NetworkStatusChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectionDetector connectionDetector = new ConnectionDetector(context);
        boolean status = connectionDetector.isConnectingToInternet();
        if (!status) {
            Toast.makeText(context, "Network Error !", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Connection re-established", Toast.LENGTH_SHORT).show();

        }
    }
}
