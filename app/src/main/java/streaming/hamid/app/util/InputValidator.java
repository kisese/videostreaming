package streaming.hamid.app.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Brayo on 6/15/2015.
 */
public class InputValidator {

    public InputValidator(){}

    //match for standard Kenyan mobile number
    public boolean validPhoneNumber(String phone_number){

        String PHONE_PATTERN = "^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$";

        Pattern pattern = Pattern.compile(PHONE_PATTERN);
        Matcher matcher = pattern.matcher(phone_number);
        return matcher.matches();
    }


    //match for a standard email address
    public boolean validEmail(String email){

        String EMAIL_PATTERN = "^[a-zA-Z0-9_\\.\\-]+@[a-zA-Z0-9\\-]+\\.[a-zA-Z0-9\\-\\.]+$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    //Match a standard name, no numbers
    public boolean validName(String name){

        String NAME_PATTERN = "^[a-zA-Z]{1,}+$";

        Pattern pattern = Pattern.compile(NAME_PATTERN);
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }

    //Match a standard text with spaces
    public boolean validText(String name){

        String NAME_PATTERN = "^[a-zA-Z ]{1,}+$";

        Pattern pattern = Pattern.compile(NAME_PATTERN);
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }


    //match a secure password with alphanumeric characters and a length of betweeen 4 and 20
    public boolean validPassword(String password){

        String PASSWORD_PATTERN = "^(?=.*\\d)(?=.*[a-zA-Z]).{4,20}$";

        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public boolean validAlternatePassword(String password){
        String PASSWORD_PATTERN = "^[a-zA-Z0-9_~!@#$%&^&*()=+-]{1,}+$";

        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public boolean validChatInput(String message){
        String NAME_PATTERN = "^[a-zA-Z0-9 ]{1,}+$";

        Pattern pattern = Pattern.compile(NAME_PATTERN);
        Matcher matcher = pattern.matcher(message);
        return matcher.matches();
    }

}
