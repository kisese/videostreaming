package streaming.hamid.app.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import streaming.hamid.app.App;
import streaming.hamid.app.R;
import streaming.hamid.app.storage.StreamDBAdapter;
import streaming.hamid.app.storage.UrlsDBAdapter;

/**
 * Created by Brayo on 5/3/2016.
 */
public class FetchStreamsAsync {
    // Tag used to cancel the request
    String tag_json_obj = "json_obj_req";
    private String TAG = "test";
    private JsonArrayRequest jsonObjReq;
    private MainActivity main_activity;
    private Context context;
    private String json;
    private String stream_id, title, logo, stream_url, info, stream_external_url = "null";
    private int active;
    private JSONArray stream_urls, stream_external_urls;
    private JSONObject stream_object;
    private JSONArray stream;
    private StreamDBAdapter streamDBAdapter;
    private UrlsDBAdapter urlsDBAdapter;
    private ProgressDialog pDialog;
    private Cursor cursor;

    public FetchStreamsAsync(Context context, MainActivity main_activity) {
        this.context = context;
        this.main_activity = main_activity;
        streamDBAdapter = new StreamDBAdapter(context);
        urlsDBAdapter = new UrlsDBAdapter(context);
    }

    public void networkOperation(String url) {
//        Cache cache = App.getInstance().getRequestQueue().getCache();
//        Cache.Entry entry = cache.get(url);
//        if (entry != null) {
//            try {
//                String data = new String(entry.data, "UTF-8");
//                // handle data, like converting it to xml, json, bitmap etc.,
//                Log.e("cached: ",  data);
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//        } else {
        // Cached response doesn't exists. Make network call here
        fetchData(url);
        //}
    }

    public void fetchData(String url) {
        final ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage("Fetching Stream...");
        pDialog.show();

        jsonObjReq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray enezaJson) {
                        Log.d(TAG, enezaJson.toString());

                        Log.e("Json String", enezaJson.toString());
                        processJson(enezaJson.toString());
                        pDialog.hide();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //test_text.setText(error.getMessage());
                VolleyLog.e("VOLLEY ERROR:::::::", "Error: " + error.toString());
                // hide the progress dialog
                if (!(main_activity == null)) {
                    if (error instanceof TimeoutError) {
                        Toast.makeText(context, "Network Timeout", Toast.LENGTH_SHORT).show();
                        Log.e("ERROR ERROR", error.getLocalizedMessage() + " " +
                                error.getMessage() + " " + error.getMessage());
                        error.printStackTrace();
                    } else if (error instanceof NoConnectionError) {
                        Toast.makeText(context, "No Connection", Toast.LENGTH_SHORT).show();
                        Log.e("ERROR ERROR", error.getLocalizedMessage() + " " +
                                error.getMessage() + " " + error.getMessage());
                        error.printStackTrace();
                    } else if (error instanceof AuthFailureError) {
                        Log.e("ERROR ERROR", error.getLocalizedMessage() + " " +
                                error.getMessage() + " " + error.getMessage());
                        error.printStackTrace();
                    } else if (error instanceof ServerError) {
                        Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
                        Log.e("ERROR ERROR", error.getLocalizedMessage() + " " +
                                error.getMessage() + " " + error.getMessage());
                        error.printStackTrace();
                    } else if (error instanceof NetworkError) {
                        Toast.makeText(context, "Network Error", Toast.LENGTH_SHORT).show();
                        Log.e("ERROR ERROR", error.getLocalizedMessage() + " " +
                                error.getMessage() + " " + error.getMessage());
                        error.printStackTrace();
                    } else if (error instanceof ParseError) {
                        Toast.makeText(context, "Parse Error", Toast.LENGTH_SHORT).show();
                        Log.e("ERROR ERROR", error.getLocalizedMessage() + " " +
                                error.getMessage() + " " + error.getMessage());
                        error.printStackTrace();
                    }

                    jsonObjReq.cancel();
                }
                pDialog.hide();
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                0, -1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        App.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void processJson(String json_string) {
        try {
            stream = new JSONArray(json_string);

            for (int i = 0; i < stream.length(); i++) {
                JSONObject stream_object = stream.getJSONObject(i);
                title = stream_object.getString("title");
                active = stream_object.getInt("active");
                logo = stream_object.getString("logo");
                info = stream_object.getString("info");

                streamDBAdapter.insertStream(title, String.valueOf(active), logo, info);

                if (stream_object.has("external_link")) {
                    stream_external_urls = stream_object.getJSONArray("external_link");
                    for (int j = 0; j < stream_external_urls.length(); j++) {
                        stream_external_url = stream_external_urls.get(j).toString();
                    }
                }

                stream_urls = stream_object.getJSONArray("stream_url");
                for (int j = 0; j < stream_urls.length(); j++) {
                    stream_url = stream_urls.get(j).toString();
                    if (stream_object.has("external_link")) {
                        urlsDBAdapter.insertStream(title, stream_url, stream_external_url);
                    } else {
                        urlsDBAdapter.insertStream(title, stream_url, "null");
                    }
                    Log.e("Strem url", stream_url);
                }
            }

            main_activity.channelsListAdapter.notifyDataSetChanged();
            main_activity.streams_list.invalidateViews();
            //refresh the viewpager

            cursor = streamDBAdapter.queryAllStreams();
            main_activity.cursor = streamDBAdapter.queryAllStreams();
            main_activity.channelsListAdapter = new ChannelsListAdapter(context, R.layout.channels_row,
                    cursor, main_activity.from, main_activity.to);
            main_activity.streams_list.setAdapter(main_activity.channelsListAdapter);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
}
