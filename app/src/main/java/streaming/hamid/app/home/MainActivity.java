package streaming.hamid.app.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;

import streaming.hamid.app.R;
import streaming.hamid.app.storage.DBOpenHelper;
import streaming.hamid.app.storage.StreamDBAdapter;
import streaming.hamid.app.storage.UrlsDBAdapter;
import streaming.hamid.app.util.ActivityStatus;
import streaming.hamid.app.util.ConnectionDetector;
import streaming.hamid.app.util.GetDate;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener,
        SearchView.OnCloseListener {

    public StreamDBAdapter streamDBAdapter;
    private UrlsDBAdapter urlDbAdapter;
    private AdView mAdView;
    public Cursor cursor;
    public ChannelsListAdapter channelsListAdapter;
    DBOpenHelper helper_ob;
    public String[] from = {helper_ob.KEY_TITLE};
    public int[] to = {R.id.stream_title};
    public ListView streams_list;
    public SearchView searchView;
    private String URL_ = "http://ganjaapps.altervista.org/StreamingApp/link.json";
    private FetchStreamsAsync fetchStreamsAsync;
    private ConnectionDetector connectionDetector;
    long a_, b_;
    private ActivityStatus activityStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setSubtitle("Find something to stream");
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        streamDBAdapter = new StreamDBAdapter(this);
        connectionDetector = new ConnectionDetector(this);
        urlDbAdapter = new UrlsDBAdapter(this);
        activityStatus = new ActivityStatus(this);

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
                .addTestDevice("705C3D5DBC14D8EADD3B9107816BD501")
                .build();
        mAdView.loadAd(adRequest);

        //have cool parralax viewpage
        streams_list = (ListView) findViewById(R.id.streams_listview);
        searchView = (SearchView) findViewById(R.id.search);
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);
        searchView.clearFocus();


        //clear the db tables if new activity
        if (activityStatus.getStatus() == false) {
            a_ = streamDBAdapter.deleteRecordS();
            b_ = urlDbAdapter.deleteRecordS();
        }

        Log.e("Delete Response", a_ + " " + b_);

        try {
            if (streamDBAdapter.getStreamRowCount() == 0) {
                //new ReadStreamsAsync(this, this).execute();
                if (connectionDetector.isConnectingToInternet()) {
                    fetchStreamsAsync = new FetchStreamsAsync(this, this);
                    fetchStreamsAsync.networkOperation(URL_);
                    activityStatus.setStatus(true);
                } else {
                    Toast.makeText(MainActivity.this, "Please check our data connection", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        cursor = streamDBAdapter.queryAllStreams();
        channelsListAdapter = new ChannelsListAdapter(MainActivity.this, R.layout.channels_row,
                cursor, from, to);
        streams_list.setAdapter(channelsListAdapter);

        streams_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor listCursor = (Cursor) parent.getItemAtPosition(position);
                String stream_name = listCursor.getString(listCursor.getColumnIndex(helper_ob.KEY_TITLE));

                Intent i = new Intent(MainActivity.this, DetailActivity.class);
                i.putExtra("stream_name", stream_name);
                startActivity(i);
            }
        });
    }

    @Override
    public boolean onClose() {
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        showResults(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        showResults(newText);
        return false;
    }

    private void showResults(String query) {

        Cursor cursor = streamDBAdapter.queryStreams(query);

        if (cursor != null) {
            streams_list.invalidateViews();
            channelsListAdapter = new ChannelsListAdapter(MainActivity.this, R.layout.channels_row,
                    cursor, from, to);
            streams_list.setAdapter(channelsListAdapter);
        }

        //    searchView.clearFocus();
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        activityStatus.setStatus(false);
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        activityStatus.setStatus(false);
        super.onDestroy();
    }
}
