package streaming.hamid.app.home;

import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import streaming.hamid.app.R;
import streaming.hamid.app.storage.UrlsDBAdapter;
import tcking.github.com.giraffeplayer.GiraffePlayer;
import tv.danmaku.ijk.media.player.IMediaPlayer;

public class VideoPlayerActivity extends AppCompatActivity implements View.OnTouchListener {

    GiraffePlayer player;
    String stream_name;
    ArrayList<String> urls = new ArrayList<>();
    UrlsDBAdapter urlsDBAdapter;
    UrlsListAdapter urlsListAdapter;
    ListView urls_list;
    private Handler handler;
    private int index;
    private boolean completed, error;
    private String external_url = " ";
    boolean pressed = false;
    private CoordinatorLayout screen_layout;
    private ArrayList<String> channel_names = new ArrayList<>();
    private ArrayList<String> urls_ = new ArrayList<>();
    private Spinner url_spinner;
    private String selected_url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        Intent i = getIntent();
        stream_name = i.getStringExtra("stream_name");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(stream_name);
        setSupportActionBar(toolbar);

        if (toolbar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        urlsDBAdapter = new UrlsDBAdapter(this);
        screen_layout = (CoordinatorLayout) findViewById(R.id.screen_layout);
        urls_list = (ListView) findViewById(R.id.urls_list);
        url_spinner = (Spinner) findViewById(R.id.url_spinner);

        screen_layout.setOnTouchListener(this);
        urls_list.setVisibility(View.GONE);

        urls = urlsDBAdapter.getAllUrls(stream_name);
        for (int j = 0; j < urls.size(); j++) {
            channel_names.add("Server " + (j + 1));
        }

//        urls.add("https://ia800201.us.archive.org/22/items/ksnn_compilation_master_the_internet/ksnn_compilation_master_the_internet_512kb.mp4");
//        urls.add("https://ia800201.us.archive.org/22items/ksnn_compilation_master_the_internet/ksnn_compilation_master_the_internet_512kb.mp4");

        urlsListAdapter = new UrlsListAdapter(this, R.id.url_text, urls);
        urls_list.setAdapter(urlsListAdapter);
        player = new GiraffePlayer(this);
        player.playInFullScreen(true);
        player.setFullScreenOnly(true);
        player.onControlPanelVisibilityChang(new GiraffePlayer.OnControlPanelVisibilityChangeListener() {
            @Override
            public void change(boolean isShowing) {
                if(isShowing) {
                    url_spinner.setVisibility(View.VISIBLE);
                    hideView();
                }
            }
        });

        String[] array = channel_names.toArray(new String[channel_names.size()]);
        hideView();

        ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, array);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        url_spinner.setAdapter(categoryAdapter);
        url_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = urls.get(position);
                selected_url = item;
                playBack(selected_url);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        try {
            Cursor m_cursor = urlsDBAdapter.queryExternalUrl(stream_name);
            if (m_cursor.moveToFirst()) {
                external_url = m_cursor.getString(m_cursor.getColumnIndex("external_url"));
            }
            m_cursor.close();

            Log.e("Extenal URL", external_url);

            handler = new Handler();
            index = 0;

            if (external_url.equals("null")) {
                Log.e("Url", urls.get(index));
                playBack(urls.get(index));
            } else {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(external_url)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //player.play("https://ia800201.us.archive.org/22/items/ksnn_compilation_master_the_internet/ksnn_compilation_master_the_internet_512kb.mp4");


    public void playBack(String url) {
        player.play(url);
        player.playInFullScreen(true);
        Log.e("Duration", player.getDuration() + " ");
        Log.e("Currently playing", url);
        player.onComplete(new Runnable() {
            @Override
            public void run() {
                //callback when video is finish
                //Toast.makeText(getApplicationContext(), "video play completed", Toast.LENGTH_SHORT).show();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (index < urls.size())
                            playBack(urls.get(index++));
                    }
                }, 1000 * 2);
                Log.e("Duration", player.getDuration() + " ");
            }
        }).onInfo(new GiraffePlayer.OnInfoListener() {
            @Override
            public void onInfo(int what, int extra) {
                switch (what) {
                    case IMediaPlayer.MEDIA_INFO_BUFFERING_START:
                        //do something when buffering start
                        break;
                    case IMediaPlayer.MEDIA_INFO_BUFFERING_END:
                        //do something when buffering end
                        break;
                    case IMediaPlayer.MEDIA_INFO_NETWORK_BANDWIDTH:
                        //download speed
                        break;
                    case IMediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START:
                        //do something when video rendering
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                            }
                        }, player.getDuration());
                        Log.e("Duration", player.getDuration() + " ");
                        break;
                }
            }
        }).onError(new GiraffePlayer.OnErrorListener() {
            @Override
            public void onError(int what, int extra) {
                // Toast.makeText(getApplicationContext(), "video play error", Toast.LENGTH_SHORT).show();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (index < urls.size()) {
                            playBack(urls.get(index++));
                            Toast.makeText(getApplicationContext(), "Server " + index++, Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(getApplicationContext(), "video play error", Toast.LENGTH_SHORT).show();
                    }
                }, 500);
            }
        });

        urls_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    if (external_url.equals("null")) {
                        Log.e("Playing", urls.get(position));
                        playBack(urls.get(position));
                    } else {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(external_url)));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (player != null) {
            player.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (player != null) {
            player.onResume();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (player != null) {
            player.onDestroy();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (player != null) {
            player.onConfigurationChanged(newConfig);
        }
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {

        //int action = event.getAction();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                url_spinner.setVisibility(View.VISIBLE);
                pressed = true;
                hideView();
                break;

            case MotionEvent.ACTION_MOVE:
                //User is moving around on the screen
                url_spinner.setVisibility(View.VISIBLE);
                hideView();
                break;

            case MotionEvent.ACTION_UP:
                pressed = false;
                url_spinner.setVisibility(View.VISIBLE);
                hideView();
                break;
        }
        return pressed;
    }

    public void hideView(){
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        url_spinner.setVisibility(View.GONE);
                    }
                });
            }
        };
        thread.start(); //start the thread
    }
}
