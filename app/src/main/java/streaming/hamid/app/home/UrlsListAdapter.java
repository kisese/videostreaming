package streaming.hamid.app.home;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import streaming.hamid.app.R;

/**
 * Created by Brayo on 5/1/2016.
 */
public class UrlsListAdapter extends ArrayAdapter<String> {

    private final LayoutInflater inflater;
    private final Typeface type_2;
    HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();
    private Context context;
    private List<String> urls = new ArrayList<>();
    private  ColorGenerator generator;
    private int color;
    private TextDrawable drawable;

    public UrlsListAdapter(Context context, int textViewResourceId,
                           List<String> objects) {
        super(context, textViewResourceId, objects);
        for (int i = 0; i < objects.size(); ++i) {
            mIdMap.put(objects.get(i), i);
        }

        this.urls = objects;
        this.context = context;
      // or use DEFAUL
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        type_2 = Typeface.createFromAsset(context.getAssets(), "Raleway-Medium.ttf");
    }

    @Override
    public long getItemId(int position) {
        String item = getItem(position);
        return mIdMap.get(item);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder = null;

        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.urls_row, null);
            holder.icon_left = (ImageView) view.findViewById(R.id.icon_left_url);
            holder.txt_url = (TextView) view.findViewById(R.id.url_text);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        String url = "Server " + String.valueOf(position);

        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
// generate random color
        SpannableString string = new SpannableString(url);
        string.setSpan(new UnderlineSpan(), 0, string.length(), 0);

        int color = generator.getRandomColor();
        Log.e("color", String .valueOf(color));
        holder.txt_url.setText(string);
        holder.txt_url.setTextColor(Color.BLUE);
        holder.icon_left.setBackgroundColor(color);
        holder.txt_url.setTypeface(type_2);
        return view;
    }

    public class ViewHolder{
        ImageView icon_left;
        TextView txt_url;
    }
}
