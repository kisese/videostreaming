package streaming.hamid.app.home;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import streaming.hamid.app.R;
import streaming.hamid.app.storage.DBOpenHelper;
import streaming.hamid.app.storage.StreamDBAdapter;
import streaming.hamid.app.storage.UrlsDBAdapter;

public class DetailActivity extends AppCompatActivity {

    private ImageView logo_view;
    private TextView title_text, description_text, no_streams;
    private FloatingActionButton fab;
    private Typeface type;
    private String stream_name;
    private StreamDBAdapter streamDbadapter;
    private UrlsDBAdapter urlDbAdapter;
    private DBOpenHelper dbOpenHelper;
    private String info;
    private Typeface lato_regular;
    private Typeface lato_bold;
    private Typeface raleway;
    private Cursor count_cursor;
    private AdView mAdView;
    private String TAG = DetailActivity.class.getSimpleName();
    InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mAdView = (AdView) findViewById(R.id.adView);

        if (toolbar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        Intent i = getIntent();
        stream_name = i.getStringExtra("stream_name");

        streamDbadapter = new StreamDBAdapter(this);
        urlDbAdapter = new UrlsDBAdapter(this);

        type = Typeface.createFromAsset(getAssets(), "28 Days Later.ttf");
        lato_regular = Typeface.createFromAsset(getAssets(), "Lato-Bold.ttf");
        lato_bold = Typeface.createFromAsset(getAssets(), "Lato-Regular.ttf");
        raleway = Typeface.createFromAsset(getAssets(), "Raleway-Medium.ttf");

        logo_view = (ImageView) findViewById(R.id.logo_view);
        title_text = (TextView) findViewById(R.id.title_text);
        no_streams = (TextView) findViewById(R.id.no_streams);
        description_text = (TextView) findViewById(R.id.description_text);
        fab = (FloatingActionButton) findViewById(R.id.fab);


        //show ad
        mInterstitialAd = new InterstitialAd(this);

        // set the ad unit ID
        mInterstitialAd.setAdUnitId(getString(R.string.HugeAd));

//        AdRequest adRequest = new AdRequest.Builder()
//                .build();

        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
                .addTestDevice("705C3D5DBC14D8EADD3B9107816BD501")
                .build();

        // Load ads into Interstitial Ads
        mInterstitialAd.loadAd(adRequest);
        mAdView.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                showInterstitial();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(DetailActivity.this, VideoPlayerActivity.class);
                i.putExtra("stream_name", stream_name);
                startActivity(i);
            }
        });

        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        int color = generator.getRandomColor();
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .useFont(type)
                .endConfig()
                .buildRect(stream_name, color);

        fab.setBackgroundTintList(ColorStateList.valueOf(color));
        toolbar.setBackgroundColor(color);

        logo_view.setImageDrawable(drawable);
        title_text.setText(stream_name);

        Cursor c = streamDbadapter.queryStreams(stream_name);
        while (c.moveToNext()) {
            info = c.getString(c.getColumnIndex(dbOpenHelper.KEY_INFO));
        }
        c.close();

        if (info.length() > 0) {
            final SpannableString ss = new SpannableString(info);
            ss.setSpan(new RelativeSizeSpan(2f), 0, 1, 0);
            description_text.setText(info);
        }

        count_cursor = urlDbAdapter.queryUrlNos(stream_name);
        no_streams.setText(String.valueOf(count_cursor.getCount()) + " Streams");

        description_text.setTypeface(lato_bold);
        title_text.setTypeface(lato_bold);
        no_streams.setTypeface(raleway);
    }

    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }
}
