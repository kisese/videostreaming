package streaming.hamid.app.home;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import streaming.hamid.app.R;
import streaming.hamid.app.storage.UrlsDBAdapter;

/**
 * Created by Brayo on 4/29/2016.
 */
public class ChannelsListAdapter extends SimpleCursorAdapter {

    private final ColorGenerator generator;
    private final Typeface type;
    private final Typeface type_2;
    TextView stream_title, stream_no;
    ImageView icon_image, icon_left;
    ImageView active_image;
    private int color;
    private TextDrawable drawable;
    UrlsDBAdapter urlsDBAdapter;
    String no_urls;
    private Cursor count_cursor;
    private int lastPosition = -1;

    public ChannelsListAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
        super(context, layout, c, from, to);
        generator = ColorGenerator.DEFAULT; // or use DEFAUL
        urlsDBAdapter = new UrlsDBAdapter(context);
        type = Typeface.createFromAsset(context.getAssets(), "Lato-Bold.ttf");
        type_2 = Typeface.createFromAsset(context.getAssets(), "Raleway-Medium.ttf");
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        color = generator.getRandomColor();
        stream_title = (TextView) view.findViewById(R.id.stream_title);
        stream_no = (TextView) view.findViewById(R.id.stream_no);
        icon_image = (ImageView) view.findViewById(R.id.icon_image);
        active_image = (ImageView) view.findViewById(R.id.stream_active);
        icon_left = (ImageView) view.findViewById(R.id.icon_left);

        int position = cursor.getPosition();
        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        view.startAnimation(animation);
        lastPosition = position;

        String title_text = cursor.getString(1);
        int active = Integer.valueOf(cursor.getString(2));

        String first_char = title_text.substring(0, Math.min(title_text.length(),1)).toUpperCase();
        drawable = TextDrawable.builder()
                .buildRoundRect(first_char, Color.DKGRAY, 10);

        icon_image.setImageDrawable(drawable);
        stream_title.setText(title_text);

        icon_left.setBackgroundColor(color);

        count_cursor = urlsDBAdapter.queryUrlNos(title_text);
        stream_no.setText(String.valueOf(count_cursor.getCount()) + " Streams");

        if (active == 1) {
            active_image.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_online));
        } else
            active_image.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_offline));

        stream_title.setTypeface(type);
        stream_no.setTypeface(type_2);
    }
}
