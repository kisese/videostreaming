package streaming.hamid.app.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Brayo on 2/9/2016.
 */
public class DBOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "STREAM_DB";

    public static final String STREAMS_TABLE = "streams_table";
    public static final String STREAMS_URLS_TABLE = "streams_urls_table";

    public static final int VERSION = 4;

    public static final String KEY_ID = "_id";
    public static final String KEY_CREATED_AT = "created_at";
    public static final String KEY_UPDATED_AT = "updated_at";
    public static final String KEY_TITLE = "title";
    public static final String KEY_ACTIVE = "active";
    public static final String KEY_INFO = "info";
    public static final String KEY_LOGO = "logo";

    public static final String KEY_URL = "url";
    public static final String KEY_EXTERNAL_URL = "external_url";
    public static final String KEY_STREAM_NAME = "stream_name";

    public static final String SCRIPT_STREAMS_TABLE = "create table " + STREAMS_TABLE + " ("
            + KEY_ID + " integer primary key autoincrement not null, " +
            KEY_TITLE + " text, " +
            KEY_ACTIVE + " text, " +
            KEY_INFO + " text, " +
            KEY_LOGO + " text, " +
            KEY_CREATED_AT + " text, " +
            KEY_UPDATED_AT + " text);";

    public static final String SCRIPT_STREAM_URLS_TABLE = "create table " + STREAMS_URLS_TABLE + " ("
            + KEY_ID + " integer primary key autoincrement not null, " +
            KEY_STREAM_NAME + " text, " +
            KEY_URL + " text, " +
            KEY_EXTERNAL_URL + " text, " +
            KEY_CREATED_AT + " text, " +
            KEY_UPDATED_AT + " text);";

    private static DBOpenHelper instance;

    public static synchronized DBOpenHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DBOpenHelper(context.getApplicationContext());
        }
        return instance;
    }

    public DBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(SCRIPT_STREAMS_TABLE);
        db.execSQL(SCRIPT_STREAM_URLS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
            if (oldVersion < 2) {
                db.execSQL("ALTER TABLE streams_urls_table ADD COLUMN external_url text");
            }
            if (oldVersion < 3) {
                db.execSQL("ALTER TABLE streams_urls_table ADD COLUMN external_url text");
            }
    }
}
