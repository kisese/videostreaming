package streaming.hamid.app.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import streaming.hamid.app.util.GetDate;

/**
 * Created by Brayo on 4/29/2016.
 */
public class StreamDBAdapter {

    SQLiteDatabase database_ob;
    DBOpenHelper openHelper_ob;
    GetDate getDate = new GetDate();
    Context context;

    String[] cols = {openHelper_ob.KEY_ID, openHelper_ob.KEY_TITLE,
            openHelper_ob.KEY_ACTIVE, openHelper_ob.KEY_LOGO, openHelper_ob.KEY_INFO,
            openHelper_ob.KEY_CREATED_AT, openHelper_ob.KEY_UPDATED_AT};

    public StreamDBAdapter(Context context) {
        this.context = context;
    }

    public StreamDBAdapter openToRead() {
        openHelper_ob = DBOpenHelper.getInstance(context);
        database_ob = openHelper_ob.getReadableDatabase();
        return this;
    }

    public StreamDBAdapter openToWrite() {
        openHelper_ob = DBOpenHelper.getInstance(context);
        database_ob = openHelper_ob.getWritableDatabase();
        return this;
    }

    public void close_db() {
        database_ob.close();
    }

    public long insertStream(String title, String active, String logo, String info) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(openHelper_ob.KEY_TITLE, title);
        contentValues.put(openHelper_ob.KEY_ACTIVE, active);
        contentValues.put(openHelper_ob.KEY_LOGO, logo);
        contentValues.put(openHelper_ob.KEY_INFO, info);
        contentValues.put(openHelper_ob.KEY_CREATED_AT, getDate.getTimeStamp());
        contentValues.put(openHelper_ob.KEY_UPDATED_AT, getDate.getTimeStamp());
        openToWrite();
        long val = database_ob.insert(openHelper_ob.STREAMS_TABLE, null,
                contentValues);
        //close_db();
        return val;
    }

    public Cursor queryAllStreams() {
        openToRead();
        Cursor c = database_ob.query(openHelper_ob.STREAMS_TABLE, cols, null,
                null, null, null, openHelper_ob.KEY_ID + " ASC");
        // //close_db();
        return c;
    }

    public int getStreamRowCount() {
        openToRead();
        Cursor c = database_ob.query(openHelper_ob.STREAMS_TABLE, cols, null,
                null, null, null, null);
        int count = c.getCount();
        //close_db();
        return count;
    }

    public Cursor queryStreams(String filter) {
        String[] selectionArgs = {"%" + filter + "%"};
        openToRead();
        Cursor c = database_ob.query(openHelper_ob.STREAMS_TABLE, cols, openHelper_ob.KEY_TITLE + " LIKE ?",
                selectionArgs, null, null, null);
        ////close_db();
        return c;
    }

    public int deleteRecordS() {
        // TODO Auto-generated method stub
        openToWrite();
        int val = database_ob.delete(openHelper_ob.STREAMS_TABLE, null, null);
       // Close();
        return val;
    }
}
