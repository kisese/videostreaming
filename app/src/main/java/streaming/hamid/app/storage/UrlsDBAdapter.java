package streaming.hamid.app.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import streaming.hamid.app.util.GetDate;

/**
 * Created by Brayo on 4/29/2016.
 */
public class UrlsDBAdapter {

    SQLiteDatabase database_ob;
    DBOpenHelper openHelper_ob;
    GetDate getDate = new GetDate();
    Context context;

    String[] cols = {openHelper_ob.KEY_ID, openHelper_ob.KEY_STREAM_NAME, openHelper_ob.KEY_URL, openHelper_ob.KEY_EXTERNAL_URL,
            openHelper_ob.KEY_CREATED_AT, openHelper_ob.KEY_UPDATED_AT};

    public UrlsDBAdapter(Context context) {
        this.context = context;
    }

    public UrlsDBAdapter openToRead() {
        openHelper_ob = DBOpenHelper.getInstance(context);
        database_ob = openHelper_ob.getReadableDatabase();
        return this;
    }

    public UrlsDBAdapter openToWrite() {
        openHelper_ob = DBOpenHelper.getInstance(context);
        database_ob = openHelper_ob.getWritableDatabase();
        return this;
    }

    public void close_db() {
        database_ob.close();
    }

    public long insertStream(String name, String url, String external_url) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(openHelper_ob.KEY_STREAM_NAME, name);
        contentValues.put(openHelper_ob.KEY_URL, url);
        contentValues.put(openHelper_ob.KEY_EXTERNAL_URL, external_url);
        contentValues.put(openHelper_ob.KEY_CREATED_AT, getDate.getTimeStamp());
        contentValues.put(openHelper_ob.KEY_UPDATED_AT, getDate.getTimeStamp());
        openToWrite();
        long val = database_ob.insert(openHelper_ob.STREAMS_URLS_TABLE, null,
                contentValues);
        //close_db();
        return val;
    }

    public Cursor queryAllStreams() {
        openToRead();
        Cursor c = database_ob.query(openHelper_ob.STREAMS_URLS_TABLE, cols, null,
                null, null, null, openHelper_ob.KEY_ID + " ASC");
        // //close_db();
        return c;
    }

    public Cursor queryExternalUrl(String filter) {
        String[] selectionArgs = {"%" + filter + "%"};
        openToRead();
        Cursor c = database_ob.query(openHelper_ob.STREAMS_URLS_TABLE, cols, openHelper_ob.KEY_STREAM_NAME + " LIKE ?",
                selectionArgs, null, null, null);
        // //close_db();
        return c;
    }

    public Cursor queryUrlNos(String filter) {
        String[] selectionArgs = {"%" + filter + "%"};
        openToRead();
        Cursor c = database_ob.query(openHelper_ob.STREAMS_URLS_TABLE, cols, openHelper_ob.KEY_STREAM_NAME + " LIKE ?",
                selectionArgs, null, null, null);
        ////close_db();
        return c;
    }

    public int getUrlRowCount() {
        openToRead();
        Cursor c = database_ob.query(openHelper_ob.STREAMS_URLS_TABLE, cols, null,
                null, null, null, null);
        int count = c.getCount();
        //close_db();
        return count;
    }

    public ArrayList<String> getAllUrls(String filter) {
        ArrayList<String> list = new ArrayList<String>();
        String[] selectionArgs = {"%" + filter + "%"};
        openToRead();
        Cursor c = database_ob.query(openHelper_ob.STREAMS_URLS_TABLE, cols, openHelper_ob.KEY_STREAM_NAME + " LIKE ?",
                selectionArgs, null, null, null);
        if (c.moveToFirst()) {
            do {
                list.add(c.getString(2));
            }
            while (c.moveToNext());
        }
        if (c != null && !c.isClosed()) {
            c.close();
        }

        return list;
    }

    public int deleteRecordS() {
        // TODO Auto-generated method stub
        openToWrite();
        int val = database_ob.delete(openHelper_ob.STREAMS_URLS_TABLE, null, null);
        // Close();
        return val;
    }
}
